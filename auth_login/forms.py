from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm
Genders = (
	('M', 'Mujer'),
	('H', 'Hombre'),
	('O', 'Otro'),
)


Estados = (
	('In', 'Inactivo'),
	('Ac', 'Activo'),
	('Bl', 'Bloqueado'),
)

class UserForm(forms.Form):
    username = forms.CharField(label='Username')
    rut =  forms.CharField(label='Rut')
    first_name = forms.CharField(label='First name')
    last_name = forms.CharField(label='Last name')
    gender = forms.ChoiceField(label='gender', choices=Genders)
    estado = forms.ChoiceField(label='estado', choices=Estados)
    age = forms.IntegerField()
    birth_date = forms.DateField(label='Birth date', widget=forms.DateInput, help_text="e.g. YYYY-MM-DD")
    email = forms.EmailField(label='E-Mail')
    password =  forms.CharField(label='Password', widget=forms.PasswordInput, max_length=100) 

class EditProfileForm(UserChangeForm):
    def __init__(self, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        del self.fields['password']

    class Meta:
        model = User
        fields = {
            'email',
            'first_name',
            'last_name',
            'password'
        }
