from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView
from django.urls import reverse_lazy
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from Transit_Hualqui.forms import MultaForm
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.db import IntegrityError
from django.core.exceptions import ObjectDoesNotExist
from Transit_Hualqui.models import (
    Apelacion,
    Auto,
    Calle,
    Multa,
    Persona,
    TipoPersona,
)


def home(request):
    data = {}
    template_name = 'home.html'

    return render(request, template_name, data)

@login_required
def listTrabajadores(request):
    data = {}
    template_name = 'trabajadores.html'
    data['personas'] = Persona.objects.filter(tipo_persona=2)

    return render(request, template_name, data)

@login_required
def listUsuarios(request):
    data = {}
    template_name = 'usuarios.html'
    data['personas'] = Persona.objects.filter(tipo_persona=3)

    return render(request, template_name, data)

@login_required
def listInfracciones(request, id):
    data = {}
    template_name = 'multas_usuario.html'
    data['multas'] = Multa.objects.filter(run_infractor=id)

    return render(request, template_name, data)

@login_required
def listApelaciones(request, id):
    data = {}
    template_name = 'apelacione_usuario.html'
    data['apelaciones'] = Apelacion.objects.filter(run_persona_a=id)

    return render(request, template_name, data)
"""
@login_required
def aprobarApelacion(request, id):
    data = {}
    template_name = 'confirmarAp.html'
    apelacion = Apelacion.objects.get(pk=id)
    apelacion.estado = "Aceptado"
    apelacion.save()

    return render(request, template_name, data)
"""

@login_required
def aprobarApelacion(request, id):
    data = {}
    template_name = 'confirmarAp.html'
    data['apelacion'] = Apelacion.objects.get(pk=id)
    if request.method == 'POST':
        data['apelacion'].estado = 'Aceptado'
        data['apelacion'].save()
        return redirect('transit:usuario-list')
    return render(request, template_name, data)

@login_required
def rechazarApelacion(request, id):
    data = {}
    template_name = 'rechazarAp.html'
    data['apelacion'] = Apelacion.objects.get(pk=id)
    if request.method == 'POST':
        data['apelacion'].estado = 'Rechazado'
        data['apelacion'].save()
        return redirect('transit:usuario-list')
    return render(request, template_name, data)

@login_required
def listAutos(request, id):
    data = {}
    template_name = 'autos-usuario.html'
    data['autos'] = Auto.objects.filter(run_persona_a=id)

    return render(request, template_name, data)


def searchMulta(request):
    data = {}
    template_name = 'usuario-multa-consulta.html'
    data['form'] = MultaForm(request.POST or None)

    if request.method == 'POST':
        if data['form'].is_valid():
            try:
                rut = request.POST['rut']
                persona = Persona.objects.get(run_persona=rut)
                
                messages.add_message(
                    request,
                    messages.SUCCESS,
                    'Consulta realizada con exito'
                )
                return redirect('transit:show-multas', id=persona.id)
            except ObjectDoesNotExist:
                persona = None
                messages.add_message(
                    request,
                    messages.ERROR,
                    'Rut no registra multas',
                )

            except IntegrityError as ie:
                messages.add_message(
                    request,
                    messages.ERROR,
                    'Problemas al realizar la consulta: {error}.'.format(
                        error=str(ie))
                )
            
        else:
            messages.add_message(
                request,
                messages.ERROR,
                'Problemas al realizar la consulta',
            )
        
    return render(request, template_name, data)

def showMultas(request, id):
    data = {}
    template_name = 'usuario-multa.html'
    try:
        data['multas'] = Multa.objects.filter(run_infractor=id)
        messages.add_message(
            request,
            messages.SUCCESS,
            'Consulta realizada con exito'
        )
    except ObjectDoesNotExist:
        data['multas'] = None
        messages.add_message(
            request,
            messages.ERROR,
            'Rut no registra multas',
        )

    return render(request, template_name, data)


@method_decorator(login_required, name='dispatch')
class PersonaListView(ListView):
    model = Persona
    template_name = 'listado_persona.html'
    context_object_name = 'personas'
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super(PersonaListView, self).get_context_data(**kwargs)
        personas = self.get_queryset()
        page = self.request.GET.get('page')
        paginator = Paginator(personas, self.paginate_by)
        try:
            personas = paginator.page(page)
        except PageNotAnInteger:
            personas = paginator.page(1)
        except EmptyPage:
            personas = paginator.page(paginator.num_pages)
        context['personas'] = personas
        return context

@method_decorator(login_required, name='dispatch')
class PersonaCreateView(CreateView):
    model = Persona
    template_name = 'create.html'
    fields = ('gender', 'age', 'run_persona', 'fecha_nacimiento', 'tipo_persona')
    success_url = reverse_lazy('transit:persona-list')

@method_decorator(login_required, name='dispatch')
class PersonaDetailView(DetailView):
    model = Persona
    template_name = 'detail.html'
    context_object_name = 'personas'

@method_decorator(login_required, name='dispatch')
class PersonaUpdateView(UpdateView):
    model = Persona
    template_name = 'update.html'
    context_object_name = 'personas'
    fields = ('gender', 'age', 'run_persona', 'fecha_nacimiento', 'tipo_persona')

    def get_success_url(self):
        return reverse_lazy('transit:persona-detail', kwargs={'pk': self.object.id})

@login_required
def DeletePersona(request, id):
    data = {}
    template_name = 'delete.html'
    data['persona'] = Persona.objects.get(pk=id)
    if request.method == 'POST':
        data['persona'].estado = 'In'
        data['persona'].save()
        return redirect('transit:persona-list')
    return render(request, template_name, data)


@method_decorator(login_required, name='dispatch')
class MultaListView(ListView):
    model = Multa
    template_name = 'listado_multa.html'
    context_object_name = 'multas'
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super(MultaListView, self).get_context_data(**kwargs)
        multas = self.get_queryset()
        page = self.request.GET.get('page')
        paginator = Paginator(multas, self.paginate_by)
        try:
            multas = paginator.page(page)
        except PageNotAnInteger:
            multas = paginator.page(1)
        except EmptyPage:
            multas = paginator.page(paginator.num_pages)
        context['multas'] = multas
        return context

@method_decorator(login_required, name='dispatch')
class MultaCreateView(CreateView):
    model = Multa
    template_name = 'create-multa.html'
    fields = ('run_infractor',
    'run_trabajador',
    'patente_m',
    'fecha_hr',
    'monto',
    'tipo_monto',
    'id_via_m',
    'gravedad',
    )
    success_url = reverse_lazy('transit:multa-list')

@method_decorator(login_required, name='dispatch')
class MultaDetailView(DetailView):
    model = Multa
    template_name = 'detail-multa.html'
    context_object_name = 'multas'

@method_decorator(login_required, name='dispatch')
class MultaUpdateView(UpdateView):
    model = Multa
    template_name = 'update_multa.html'
    context_object_name = 'multas'
    fields = ('run_infractor',
    'run_trabajador',
    'patente_m',
    'fecha_hr',
    'monto',
    'tipo_monto',
    'id_via_m',
    'gravedad',
    )

    def get_success_url(self):
        return reverse_lazy('transit:multa-detail', kwargs={'pk': self.object.id})

@method_decorator(login_required, name='dispatch')
class MultaDeleteView(DeleteView):
    model = Multa
    template_name = 'delete-multa.html'
    success_url = reverse_lazy('transit:multa-list')


##REVISARRRRRR AL LLEGAR A SANTIAGO
@method_decorator(login_required, name='dispatch')
class AutoListView(ListView):
    model = Auto
    template_name = 'listado_auto.html'
    context_object_name = 'autos'
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super(AutoListView, self).get_context_data(**kwargs)
        autos = self.get_queryset()
        page = self.request.GET.get('page')
        paginator = Paginator(autos, self.paginate_by)
        try:
            autos = paginator.page(page)
        except PageNotAnInteger:
            autos = paginator.page(1)
        except EmptyPage:
            autos = paginator.page(paginator.num_pages)
        context['autos'] = autos
        return context

@method_decorator(login_required, name='dispatch')
class AutoCreateView(CreateView):
    model = Auto
    template_name = 'create-auto.html'
    fields = ('patente', 'run_persona_a')

    success_url = reverse_lazy('transit:auto-list')

@method_decorator(login_required, name='dispatch')
class AutoDetailView(DetailView):
    model = Auto
    template_name = 'detail-auto.html'
    context_object_name = 'autos'

@method_decorator(login_required, name='dispatch')
class AutoUpdateView(UpdateView):
    model = Auto
    template_name = 'update_auto.html'
    context_object_name = 'autos'
    fields = ('patente', 'run_persona_a')

    def get_success_url(self):
        return reverse_lazy('transit:auto-detail', kwargs={'pk': self.object.id})

@method_decorator(login_required, name='dispatch')
class AutoDeleteView(DeleteView):
    model = Auto
    template_name = 'delete-auto.html'
    success_url = reverse_lazy('transit:auto-list')

##REVISARRRRRR AL LLEGAR A SANTIAGO
@method_decorator(login_required, name='dispatch')
class ApelacionListView(ListView):
    model = Apelacion
    template_name = 'listado_apelacion.html'
    context_object_name = 'apelaciones'
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super(ApelacionListView, self).get_context_data(**kwargs)
        apelaciones = self.get_queryset()
        page = self.request.GET.get('page')
        paginator = Paginator(apelaciones, self.paginate_by)
        try:
            apelaciones = paginator.page(page)
        except PageNotAnInteger:
            apelaciones = paginator.page(1)
        except EmptyPage:
            apelaciones = paginator.page(paginator.num_pages)
        context['apelaciones'] = apelaciones
        return context

@method_decorator(login_required, name='dispatch')
class ApelacionCreateView(CreateView):
    model = Apelacion
    template_name = 'create-apelacion.html'
    fields = ('id_multa_a',
    'run_persona_a',
    'descripcion',
    'estado',
    'fecha_hr_a',
    )
    success_url = reverse_lazy('transit:apelacion-list')

@method_decorator(login_required, name='dispatch')
class ApelacionDetailView(DetailView):
    model = Apelacion
    template_name = 'detail-apelacion.html'
    context_object_name = 'apelaciones'

@method_decorator(login_required, name='dispatch')
class ApelacionUpdateView(UpdateView):
    model = Apelacion
    template_name = 'update_apelacion.html'
    context_object_name = 'apelaciones'
    fields = ('id_multa_a',
    'run_persona_a',
    'descripcion',
    'estado',
    'fecha_hr_a',
    )

    def get_success_url(self):
        return reverse_lazy('transit:auto-detail', kwargs={'pk': self.object.id})

@method_decorator(login_required, name='dispatch')
class ApelacionDeleteView(DeleteView):
    model = Apelacion
    template_name = 'delete-apelacion.html'
    success_url = reverse_lazy('transit:apelacion-list')

##REVISARRRRRR AL LLEGAR A SANTIAGO
@method_decorator(login_required, name='dispatch')
class CalleListView(ListView):
    model = Calle
    template_name = 'listado_calle.html'
    context_object_name = 'calles'
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super(CalleListView, self).get_context_data(**kwargs)
        calles = self.get_queryset()
        page = self.request.GET.get('page')
        paginator = Paginator(calles, self.paginate_by)
        try:
            calles = paginator.page(page)
        except PageNotAnInteger:
            calles = paginator.page(1)
        except EmptyPage:
            calles = paginator.page(paginator.num_pages)
        context['calles'] = calles
        return context

@method_decorator(login_required, name='dispatch')
class CalleCreateView(CreateView):
    model = Calle
    template_name = 'create-calle.html'
    fields = ('id_via',
    'nombre_via',
    'sentido',
    'velocidad_maxima',
    )
    success_url = reverse_lazy('transit:calle-list')

@method_decorator(login_required, name='dispatch')
class CalleDetailView(DetailView):
    model = Calle
    template_name = 'detail-calle.html'
    context_object_name = 'calles'

@method_decorator(login_required, name='dispatch')
class CalleUpdateView(UpdateView):
    model = Calle
    template_name = 'update_calle.html'
    context_object_name = 'calles'
    fields = ('id_via',
    'nombre_via',
    'sentido',
    'velocidad_maxima',
    )

    def get_success_url(self):
        return reverse_lazy('transit:calle-detail', kwargs={'pk': self.object.id})

@method_decorator(login_required, name='dispatch')
class CalleDeleteView(DeleteView):
    model = Calle
    template_name = 'delete-calle.html'
    success_url = reverse_lazy('transit:calle-list')

