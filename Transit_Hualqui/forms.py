from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm
Genders = (
	('M', 'Mujer'),
	('H', 'Hombre'),
	('O', 'Otro'),
)


Estados = (
	('In', 'Inactivo'),
	('Ac', 'Activo'),
	('Bl', 'Bloqueado'),
)

class MultaForm(forms.Form):
    rut =  forms.CharField(label='Rut', help_text="Rut con el siguiente formato 1234568-9")
