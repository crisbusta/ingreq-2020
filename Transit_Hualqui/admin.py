from django.contrib import admin
from django.utils.safestring import mark_safe
from Transit_Hualqui.models import (
    Apelacion,
    Auto,
    Calle,
    Multa,
    Persona,
    TipoPersona,
    Turno
)

@admin.register(Persona)
class PersonaAdmin(admin.ModelAdmin):
    list_display = ['tipo_persona', 'run_persona']

@admin.register(Turno)
class TurnoAdmin(admin.ModelAdmin):
    list_display = ['id_turno', 'run_persona_t']

@admin.register(Apelacion)
class ApelacionAdmin(admin.ModelAdmin):
    list_display = ['id_multa_a', 'run_persona_a', 'descripcion']

@admin.register(Multa)
class MultaAdmin(admin.ModelAdmin):
    list_display = ['id', 'fecha_hr', 'run_infractor','run_trabajador', 'monto']

@admin.register(TipoPersona)
class TipoPersonaAdmin(admin.ModelAdmin):
    list_display = ['id', 'name',]

@admin.register(Auto)
class AutoAdmin(admin.ModelAdmin):
    list_display = ['patente', 'run_persona_a']

@admin.register(Calle)
class CalleAdmin(admin.ModelAdmin):
    list_display = ['id_via', 'nombre_via']