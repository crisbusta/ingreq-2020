from django.db import models
from django.contrib.auth.models import User
class TipoPersona(models.Model):
    name = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.name

class Persona(models.Model):
    Genders = (
	    ('M', 'Mujer'),
	    ('H', 'Hombre'),
	    ('O', 'Otro'),
    )
    Estados = (
	    ('In', 'Inactivo'),
	    ('Ac', 'Activo'),
	    ('Bl', 'Bloqueado'),
    )

    # Al desplegar un CharField con un choices saldrá un cuadro de opciones
    gender = models.CharField(max_length=10, choices=Genders)
    age = models.PositiveIntegerField(default=1)
    run_persona  = models.CharField(max_length=12, unique=True, default=1)
    fecha_nacimiento = models.DateField()
    tipo_persona = models.ForeignKey(TipoPersona, on_delete=models.CASCADE, default=1)
    user = models.OneToOneField(User, on_delete=models.CASCADE, default=1)
    estado = models.CharField(max_length=10, choices=Estados)
    def __str__(self):
        return self.run_persona

    def gender_verbose(self):
        return dict(Persona.Genders)[self.gender]

    def estado_verbose(self):
        return dict(Persona.Estados)[self.estado]


class Auto(models.Model):
    patente = models.CharField(max_length=10, unique=True)
    run_persona_a  = models.ForeignKey(Persona, on_delete=models.CASCADE, default=1)

    def __str__(self):
        return self.patente


class Calle(models.Model):
    Sentidos = (
        ('Am', 'Ambos'), 
        ('Un', 'Unico'),
    )
    id_via = models.PositiveIntegerField()
    nombre_via = models.CharField(max_length=10, unique=True)
    sentido = models.CharField(max_length=20, choices=Sentidos)
    velocidad_maxima = models.PositiveIntegerField()

    def __str__(self):
        return self.nombre_via

    def sentido_verbose(self):
        return dict(Calle.Sentidos)[self.sentido]


class Multa(models.Model):
    RangoInfraccion = (
	('G', 'Grave'),
	('Gr', 'Gravisima'),
	('O', 'Otro'),
)
    run_infractor  = models.ForeignKey(Persona, on_delete=models.CASCADE, related_name='rut_infractor', default=1)
    run_trabajador = models.ForeignKey(Persona, on_delete=models.CASCADE, default=1)
    patente_m = models.ForeignKey(Auto, on_delete=models.CASCADE,default=1)
    fecha_hr = models.DateTimeField()
    monto = models.PositiveIntegerField()
    tipo_monto = models.CharField(max_length=200)
    id_via_m = models.ForeignKey(Calle, on_delete=models.CASCADE, default=1)
    gravedad = models.CharField(max_length=200, choices=RangoInfraccion)

    def __str__(self):
        return str(self.id)

    def gravedad_verbose(self):
        return dict(Multa.RangoInfraccion)[self.gravedad]

class Apelacion(models.Model):
    id_multa_a = models.ForeignKey(Multa, on_delete=models.CASCADE)
    run_persona_a = models.ForeignKey(Persona, on_delete=models.CASCADE)
    descripcion = models.TextField(max_length=1000)
    estado = models.CharField(max_length=15)
    fecha_hr_a = models.DateTimeField()

    def __str__(self):
        return str(self.id_multa_a)
