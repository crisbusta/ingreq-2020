FROM mcantillana/django_unab:3.0.1

ADD requirements.txt /code
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
Run pip install bootstrap4
ADD . /code
